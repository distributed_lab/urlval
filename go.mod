module gitlab.com/distributed_lab/urlval/v4

go 1.16

require (
	github.com/pkg/errors v0.9.1
	github.com/spf13/cast v1.5.0
	github.com/stretchr/testify v1.7.1
	gitlab.com/distributed_lab/ape v1.6.1
	gitlab.com/distributed_lab/logan v3.8.0+incompatible
	golang.org/x/sys v0.0.0-20220520151302-bc2c85ada10a // indirect
)
